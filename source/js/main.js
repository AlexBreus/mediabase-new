//@prepros-append toggles.js

let _ = (events, target, func) => {
	events.split(' ').forEach((event) => {
		document.addEventListener(event, (e) => {
			document.querySelectorAll(target).forEach((item) => {
				let element = e.target;
				if (item == element)
					return func(e, element);
				else {
					while (element.parentElement) {
						if (item == element) {
							return func(e, element);
						}
						else
							element = element.parentElement;
					}
				}
			});
			return false;
		});
	});
};


document.addEventListener('DOMContentLoaded', () => {
	const CatalogContainer = document.querySelector('.catalog__list');
	if(CatalogContainer && [...CatalogContainer.children].length <= 4){
		CatalogContainer.classList.add('justify')
	}
	if (CatalogContainer) {
		CatalogContainer.classList.remove('row', 'column');
		if (window.innerWidth > 768) {
			CatalogContainer.classList.add('column');
		} else {
			CatalogContainer.classList.add('row');
		}
	}
	const CatalogToggle = document.querySelector('.catalog__viewtoggles').children;
	const removeActiveClass = () => {
		[...CatalogToggle].forEach(x => x.classList.remove('active'))
	}
	if (CatalogToggle) {
		[...CatalogToggle].forEach(x => {
			x.onclick = () => {
				CatalogContainer.classList.remove('row', 'column');
				CatalogContainer.classList.add(x.dataset.view);
				removeActiveClass();
				x.classList.add('active');
			}
		})
	}
	window.onresize = () => {
		if (window.innerWidth < 768) {
			CatalogContainer.classList.remove('row', 'column');
		}
	}
});