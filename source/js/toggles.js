document.addEventListener('DOMContentLoaded', () => {
	let catToggle = document.querySelector('.catalog__cat');
	let catList = document.querySelector('.catalog__nav');
	if (catToggle && catList) {
		catToggle.addEventListener('click', (e) => {
			catList.classList.toggle('active');
		});
	};
	let searchToggle = document.querySelector('.nav__search--action');
	if (searchToggle) {
		searchToggle.addEventListener('click', () => {
			if (document.querySelector('.nav__search').classList.contains('active')) {
				document.querySelector('.nav__search input').blur();
			} else {
				document.querySelector('.nav__search input').focus();
			}
			document.querySelector('.nav__logo').classList.toggle('hidden');
			document.querySelector('.nav__search').classList.toggle('active');
		});
	};

	let hoverItems = document.querySelectorAll('.catalog__list--bottom');
	// if(hoverItems){
	// 	hoverItems.forEach(item=>item.addEventListener())
	// }
})